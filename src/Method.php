<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter;
use JoaquinV\Chatter\result\Result;

/**
 * Represents a Telegram method.
 */
class Method{

  private $method = '';
  private $params = [];

  /**
   * Object constructor.
   *
   * @param string $method Method name.
   *
   * @param array  $params Parameters to set initially (more can be set
   * later.)
   */
  public function __construct(string $method, array $params=[]){
    $this->method = $method;
    foreach($params as $param => $value)
      $this->setParameter($param, $value);
  }

  /**
   * Sets a parameter for the method.
   * If a Result object is given and it has a getID() method, this method will
   * store the ID for that Result. If an array is given, it's JSON-serialized
   * as a string.
   *
   * @param string $param - Parameter name.
   * @param mixed  $value - Parameter value.
   */
  public function setParameter(string $param, $value){
    // Check if value is valid.
    switch($type = gettype($value)){
      case 'string': case 'integer': case 'bool': case 'double':
        // Do nothing since it is valid.
      break;
      case 'array':
        // Convert array to JSON.
        $value = json_encode($value, true);
      break;
      case 'object':
        if($value instanceof Result){
          if(method_exists($value, 'getID'))
            $value = $value->getID();
          else
            throw new TelegramException(
              'Invalid parameter type: object('.get_class($value).')'
            );
        }
        elseif($value instanceof InputFile){
          // Do nothing since it is valid.
        }
        else
          throw new TelegramException(
            'Invalid parameter type: object('.get_class($value).')'
          );
      break;
      default:
        throw new TelegramException(
          'Invalid parameter type: '.$type
        );
      break;
    }

    $this->params[$param] = $value;
  }

  /**
   * Re-sets the method's name.
   *
   * @param string $method Method name.
   */
  public function setMethod(string $method){
    $this->method = 'method';
  }

  /**
   * Returns the method name.
   * @return string
   */
  public function getMethod(): string{
    return $this->method;
  }

  /**
   * Returns the parameters of the method.
   * @return mixed[]
   */
  public function getParameters(): array{
    return $this->params;
  }

  /**
   * Returns a JSON-serialized representation used for direct output via
   * webhooks.
   * 
   * @return string
   */
  public function getWebhookReply(): string{
    $json = ['method'=>$this->method] + $this->params;
    return json_encode($json);
  }
}
