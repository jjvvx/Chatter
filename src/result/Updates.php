<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * An array of updates that Telegram's getUpdates method returns.
 */
class Updates extends ResultArray{

	public function __construct(array $updates){
		foreach($updates as $update)
			$this->array[] = new Update($update);
	}
}
