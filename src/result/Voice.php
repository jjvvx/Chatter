<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a voice file, sometimes called a "voice note".
 */
class Voice extends Result{

	protected $ID='';
	protected $duration=0;
	protected $fileMIME;
	protected $fileSize;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID       = $json['file_id'];
		$this->duration = $json['duration'];
		if(isset($json['mime_type']))
			$this->fileMIME = $json['mime_type'];
		if(isset($json['file_size']))
			$this->fileSize = $json['file_size'];
	}

	public function getID(): string{
		return $this->ID;
	}

	public function getDuration(): int{
		return $this->duration;
	}

	public function getMIME(){
		return $this->fileMIME;
	}

	public function getSize(){
		return $this->fileSize;
	}
}
