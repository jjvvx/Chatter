<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a document.
 */
class Document extends Result{

	protected $ID;
	protected $thumb;
	protected $fileName;
	protected $fileMIME;
	protected $fileSize;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID = $json['file_id'];

		if(isset($json['thumb']))
			$this->thumb = new PhotoSize($json['thumb']);
		if(isset($json['file_name']))
			$this->fileName = $json['file_name'];
		if(isset($json['mime_type']))
			$this->fileMIME = $json['mime_type'];
		if(isset($json['file_size']))
			$this->fileSize = $json['file_size'];
	}

	public function getID(): string{
		return $this->ID;
	}

	/**
	 * Gets the thumbnail of the file if it has one.
	 * 
	 * @return PhotoSize|null
	 */
	public function getThumb(){
		return $this->thumb;
	}

	public function getFilename(){
		return $this->fileName;
	}

	public function getMIME(){
		return $this->fileMIME;
	}

	public function getSize(){
		return $this->fileSize;
	}
}
