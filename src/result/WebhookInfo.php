<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents the output of a webhook.
 */
class WebhookInfo extends Result{

  private $url;
  private $ccert;
  private $pend;
  private $lasterr;
  private $lastmsg;
  private $maxconn;
  private $allowed;

  public function __construct($json){
		$json = $this->parseJSON($json);
    $this->url   = $json['url'];
    $this->ccert = $json['has_custom_certificate'];
    $this->pend  = $json['pending_update_count'];

    $this->lasterr = $json['last_error_date'] ?? null;
    $this->lastmsg = $json['last_error_message'] ?? null;
    $this->maxconn = $json['max_connections'] ?? null;
    $this->allowed = $json['allowed_updates'] ?? null;
  }

  public function getURL(): string{
    return $this->url;
  }

  public function isCustomCert(): bool{
    return $this->ccert;
  }

  public function getPending(): int{
    return $this->pend;
  }

  public function lastErrorDate(){
    return $this->lasterr;
  }

  public function lastErrorMessage(){
    return $this->lastmsg;
  }

  public function getMaxConnections(){
    return $this->maxconn;
  }

  public function getAllowedUpdates(){
    return $this->allowed;
  }
}
