<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents an audio file, usually used to send music.
 */
class Audio extends Result{

	protected $ID='';
	protected $duration=0;
	protected $performer='N/A';
	protected $title='N/A';
	protected $fileMIME;
	protected $fileSize;

	public function __construct($json){
		$json = $this->parseJSON($json);

		$this->ID       = $json['file_id'];
		$this->duration = $json['duration'];

		if(isset($json['performer']))
			$this->performer = $json['performer'];
		if(isset($json['title']))
			$this->title = $json['title'];
		if(isset($json['mime_type']))
			$this->fileMIME = $json['mime_type'];
		if(isset($json['file_size']))
			$this->fileSize = $json['file_size'];
	}

	public function getID(): string{
		return $this->ID;
	}

	public function getDuration(): int{
		return $this->duration;
	}

	public function getPerformer(): string{
		return $this->performer;
	}

	public function getTitle(): string{
		return $this->title;
	}

	public function getMIME(){
		return $this->fileMIME;
	}

	public function getSize(){
		return $this->fileSize;
	}
}
