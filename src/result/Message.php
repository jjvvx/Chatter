<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;
use JoaquinV\Chatter\Method;

/**
 * Represents a Telegram message.
 */
class Message extends Result{

	protected $type;

	protected $user;
	protected $image;

	protected $fwd = false;
	protected $fwdFrom;
	protected $fwdDate;
	protected $fwdChat;

	protected $msgID = 0;
	protected $msgFrom;
	protected $msgTime = 0;
	protected $msgChat;
	protected $msgReplyTo;

	protected $msgText;
	protected $msgWords = array();
	protected $msgIsCommand = false;
	protected $msgCommand;

	protected $msgPhoto;
	protected $msgVideo;
	protected $msgCaption = '';

	protected $msgSticker;
	protected $msgAudio;
	protected $msgVoice;
	protected $msgContact;
	protected $msgLocation;

	protected $chatLeft;
	protected $chatJoin;

	protected $chatTitle = '';
	protected $chatPhoto;

	protected $oldGroup;
	protected $newSupergroup;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->msgID   = $json['message_id'];
		$this->msgTime = $json['date'];
		$this->msgChat = new Chat($json['chat']);

		if(isset($json['from']))
			$this->msgFrom = new User($json['from']);

		// Forwards.
		if(isset($json['forward_from'])){
			$this->fwd = true;
			$this->fwdFrom = new User($json['forward_from']);
			$this->fwdDate = $json['forward_date'];
			if(isset($json['forward_from_chat']))
				$this->fwdChat = new Chat($json['forward_from_chat']);
		}
		// Replies.
		if(isset($json['reply_to_message']))
			$this->msgReplyTo = new Message($json['reply_to_message']);

		// Text messages.
		if(isset($json['text'])){
			$this->type = 'text';
			$this->msgText  = $json['text'];
			$this->msgWords = explode(' ', $this->msgText);
			if(strpos($this->msgText, '/') === 0){
				$this->msgIsCommand = true;
				$this->msgCommand   = preg_replace('/^\//', '', $this->msgWords[0]);
			}
		}
		// Audio.
		if(isset($json['audio'])){
			$this->type = 'audio';
			$this->msgAudio = new Audio($json['audio']);
		}
		// Files. (internally called documents for whatever reason ┐('~'; )┌)
		if(isset($json['document'])){
			$this->type = 'document';
			$this->msgFile = new Document($json['document']);
		}
		// Photos.
		elseif(isset($json['photo'])){
			$this->type = 'photo';
			$this->msgPhoto = new Photo($json['photo']);
			if(isset($json['caption']))
				$this->msgCaption = $json['caption'];
		}
		// Stickers.
		elseif(isset($json['sticker'])){
			$this->type = 'sticker';
			$this->msgSticker = new Sticker($json['sticker']);
		}
		// Videos.
		elseif(isset($json['video'])){
			$this->type = 'video';
			$this->msgVideo = new Video($json['video']);
			if(isset($json['caption']))
				$this->msgCaption = $json['caption'];
		}
		// Voice messages.
		elseif(isset($json['voice'])){
			$this->type = 'voice';
			$this->msgVoice = new Voice($json['voice']);
		}
		// Contacts.
		elseif(isset($json['contact'])){
			$this->type = 'contact';
			$this->msgContact = new Contact($json['contact']);
		}
		// Locations.
		elseif(isset($json['location'])){
			$this->type = 'location';
			$this->msgLocation = new Location($json['location']);
		}

		// New chat participant.
		elseif(isset($json['new_chat_participant'])){
			$this->type = 'chat_join';
			$this->chatJoin = new User($json['new_chat_participant']);
		}
		// Chat participant left.
		elseif(isset($json['left_chat_participant'])){
			$this->type = 'chat_left';
			$this->chatLeft = new User($json['left_chat_participant']);
		}

		// New chat title.
		elseif(isset($json['new_chat_title'])){
			$this->type = 'newChatTitle';
			$this->chatTitle = $json['new_chat_title'];
		}
		// New chat photo.
		elseif(isset($json['new_chat_photo'])){
			$this->type = 'newChatPhoto';
			$this->chatPhoto = new Photo($json['new_chat_photo']);
		}
		// Chat photo deleted.
		elseif(isset($json['delete_chat_photo'])){
			$this->type = 'deleteChatPhoto';
			$this->chatPhotoDel = true;
		}
		// Group created.
		elseif(isset($json['group_chat_created']))
			$this->type = 'groupCreated';
		// Channel created.
		elseif(isset($json['channel_chat_created']))
			$this->type = 'chanCreated';
		// Supergroup upgrade.
		elseif(isset($json['supergroup_chat_created'])){
			$this->type = 'supergroup';
			$this->oldGroup      = $json['migrate_to_chat_id'];
			$this->newSupergroup = $json['migrate_from_chat_id'];
		}
	}

	/**
	 * Gets the text of a message split by spaces. Useful for commands.
	 * 
	 * @param int &$argc Argument count.
	 */
	public function getArguments(int &$argc=0): array{
		$argv = $this->msgWords;
		$argc = count($argv);
		return $argv;
	}

	/**
	 * Sets the parameters of a Method for replying to this message.
	 * 
	 * @param Method $Method
	 */
	public function mkReplyMethod(Method $Method){
		$Method->setParameter('chat_id', $this->msgChat);
		$Method->setParameter('reply_to_message_id', $this);
	}

	/**
	 * Gets the type of message.
	 */
	public function getType(){
		return $this->type;
	}

	public function getID(){
		return $this->msgID;
	}

	/** When the message was sent in a Unix timestamp format. */
	public function getTime(){
		return $this->msgTime;
	}

	/** Gets the chat this message was sent in. */
	public function getChat(){
		return $this->msgChat;
	}

	public function getForwardFrom(){
		return $this->fwdFrom;
	}

	public function getForwardDate(){
		return $this->fwdDate;
	}

	public function getForwardChat(){
		return $this->fwdChat;
	}

	public function getFrom(){
		return $this->msgFrom;
	}

	public function getFromReal(){
		return $this->fwdFrom ?? $this->msgFrom;
	}

	public function getText(){
		return $this->msgText;
	}

	public function getWords(){
		return $this->msgWords;
	}

	public function requireArguments($req){
		if(count($this->msgWords)-1 < $req)
			return null;
		return $this->msgWords;
	}

	public function isCommand(){
		return $this->msgIsCommand;
	}

	public function getCommand(){
		return $this->msgCommand;
	}

	public function getAudio(){
		return $this->msgAudio;
	}

	public function getFile(){
		return $this->msgFile;
	}

	public function getPhoto(){
		return $this->msgPhoto;
	}

	public function getVideo(){
		return $this->msgVideo;
	}

	public function getCaption(){
		return $this->msgCaption;
	}

	public function getSticker(){
		return $this->msgSticker;
	}

	public function getVoiceNote(){
		return $this->msgVoice;
	}

	public function getContact(){
		return $this->msgContact;
	}

	public function getLocation(){
		return $this->msgLocation;
	}

	public function getNewUser(){
		return $this->chatJoin;
	}

	public function getUserThatLeft(){
		return $this->chatLeft;
	}

	public function getNewTitle(){
		return $this->chatTitle;
	}

	public function getNewPhoto(){
		return $this->chatPhoto;
	}

	public function getReply(){
		return $this->msgReplyTo;
	}

	public function getTextRep(): string{
		switch($this->type){
			case 'text':
				return sprintf('<%s> %s', $this->msgFrom->getIdentifier(), $this->msgText);
			break;
			case 'photo':
				$PhotoSize = $this->msgPhoto[count($this->msgPhoto)-1];

				return sprintf('(%s) Image %dx%d',
					$this->msgFrom->getIdentifier(),
					$PhotoSize->getWidth(),
					$PhotoSize->getHeight());
			break;
			case 'document':
				$File = $this->msgFile;
				return sprintf('(%s) Document: %s', $this->msgFrom->getIdentifier(), $File->getFilename());
			break;
			case 'sticker':
				$Sticker = $this->msgSticker;
				return sprintf("(%s) Sticker %s",
					$this->msgFrom->getIdentifier(),
					$Sticker->getEmoji()
				);
			case 'contact':
				$Contact = $this->msgContact;
				return sprintf('(%s) Contact: %s (%s)',
					$this->msgFrom->getIdentifier(),
					$Contact->getName(),
					$Contact->getNumber());
			break;
			case 'chat_join':
				$NewUser = $this->chatJoin;
				$Chat    = $this->msgChat;
				return sprintf('%s has joined %s', $NewUser->getIdentifier(), $Chat->getTitle());
			break;
			case 'chat_left':
				$User = $this->chatLeft;
				$Chat = $this->msgChat;
				return sprintf('%s has left %s', $User->getIdentifier(), $Chat->getTitle());
			break;
			case 'new_chat_title':
				$Chat = $this->msgChat;
				return sprintf('(%s) New title for %s: %s', $this->msgFrom->getIdentifier(), $Chat->getTitle(), $this->chatTitle);
			break;
			default:
				return sprintf('(%s) Message type %s', $this->msgFrom->getIdentifier(), $this->type);
			break;
		}
	}
}
