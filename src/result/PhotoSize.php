<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a resolution of a photo on Telegram.
 */
class PhotoSize extends Result{

	protected $ID='';
	protected $width=1;
	protected $height=1;
	protected $fileSize;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID     = $json['file_id'];
		$this->width  = $json['width'];
		$this->height = $json['height'];
		if(isset($json['file_size']))
			$this->fileSize = $json['file_size'];
	}

	public function getID(): string{
		return $this->ID;
	}

	public function getWidth(): int{
		return $this->width;
	}

	public function getHeight(): int{
		return $this->height;
	}

	public function getSize(){
		return $this->fileSize;
	}
}
