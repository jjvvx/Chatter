<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a Telegram sticker.
 */
class Sticker extends Result{

	protected $ID='';
	// Stickers are always either 512px wide or 512px tall.
	protected $width=512;
	protected $height=512;
	protected $thumb;
	protected $fileSize;
	protected $emoji = "";

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID     = $json['file_id'];
		$this->width  = $json['width'];
		$this->height = $json['height'];
		if(isset($json['thumb']))
			$this->thumb = $json['thumb'];
		if(isset($json['file_size']))
			$this->fileSize = $json['file_size'];
		if(isset($json['emoji']))
			$this->emoji = $json['emoji'];
	}

	public function getID(): string{
		return $this->ID;
	}

	public function getWidth(): int{
		return $this->width;
	}

	public function getHeight(): int{
		return $this->height;
	}

	public function getEmoji(): string{
		return $this->emoji;
	}

	public function getThumb(){
		return $this->thumb;
	}

	public function getSize(){
		return $this->fileSize;
	}
}
