<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a file in Telegram's servers.
 */
class File extends Result{

	protected $ID='';
	protected $fileSize;
	protected $filePath;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID = $json['file_id'];
		if(isset($json['file_size']))
			$this->fileSize = $json['file_size'];
		if(isset($json['file_path']))
			$this->filePath = $json['file_path'];
	}

	public function getID(): string{
		return $this->ID;
	}

	public function getSize(){
		return $this->fileSize;
	}

	public function getPath(){
		return $this->filePath;
	}
}
