<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a user in Telegram.
 */
class User extends Result{

	const FIRST_NAME = 0;
	const LAST_NAME  = 1;
	const FULL_NAME  = 2;

	protected $ID;
	protected $userName;

	protected $firstName='';
	protected $lastName ='';
	protected $fullName='';

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID = $json['id'];
		$this->firstName = $this->fullName = $json['first_name'];
		if(isset($json['last_name'])){
			$this->lastName = $json['last_name'];
			$this->fullName .= ' '.$this->lastName;
		}
		if(isset($json['username']))
			$this->userName = $json['username'];
	}

	public function getID(){
		return $this->ID;
	}

	public function getUsername(){
		return $this->userName;
	}

	public function getName(int $type = self::FIRST_NAME): string{
		if($type == self::FIRST_NAME)
			return $this->firstName;
		elseif($type == self::LAST_NAME)
			return $this->lastName;
		elseif($type == self::FULL_NAME)
			return $this->fullName;
		else
			throw new TelegramException(
				'Argument in '.__METHOD__.' must be between 0 and 2'
			);
	}

	public function getIdentifier(){
		if(isset($this->userName))
			return '@'.$this->userName;
		else
			return $this->fullName;
	}
}
