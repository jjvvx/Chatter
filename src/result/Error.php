<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;
use JoaquinV\Chatter\TelegramException;

/**
 * Erroneous Telegram API result.
 */
class Error extends Result{

	protected $code=0;
	protected $desc='';

	public function __construct($json){
		$json = $this->parseJSON($json);
		if($json['ok'])
			throw new TelegramException('Tried to make an Error object of a non-error');

		$this->code = $json['error_code'] ?? 0;
		$this->desc = $json['description'] ?? '';
	}

	/**
	 * Makes a throwable exception of this error.
	 * @return TelegramException
	 */
	public function mkException(){
		return new TelegramException($this->desc, $this->code);
	}

	public function getCode(): int{
		return $this->code;
	}

	public function getDescription(): string{
		return $this->desc;
	}
}
