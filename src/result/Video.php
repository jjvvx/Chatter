<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a video.
 */
class Video extends Result{

	protected $ID='';
	protected $width=1;
	protected $height=1;
	protected $duration=1;
	protected $thumb;
	protected $fileMIME;
	protected $fileSize;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID       = $json['file_id'];
		$this->width    = $json['width'];
		$this->height   = $json['height'];
		$this->duration = $json['duration'];
		if(isset($json['thumb']))
			$this->thumb = new PhotoSize($json['thumb']);
		if(isset($json['mime_type']))
			$this->fileMIME = $json['mime_type'];
		if(isset($json['file_size']))
			$this->fileSize = $json['file_size'];
	}

	public function getID(): string{
		return $this->ID;
	}

	public function getWidth(): int{
		return $this->width;
	}

	public function getHeight(): int{
		return $this->height;
	}

	public function getDuration(): int{
		return $this->duration;
	}

	public function getThumb(){
		return $this->thumb;
	}

	public function getMIME(){
		return $this->fileMIME;
	}

	public function getSize(){
		return $this->fileSize;
	}
}
