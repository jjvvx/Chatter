<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents an array of a user's profile pictures.
 */
class UserProfilePhotos extends Result{

  private $count  = 0;
  private $photos = [];

  public function __construct($json){
    $json = $this->parseJSON($json);

    $this->count  = $json['total_count'];
    foreach($json['photos'] as $jph)
      $this->photos[] = new Photo($jph);
  }

  public function getCount(): int{
    return $this->count;
  }

  public function getPhotos(): array{
    return $this->photos;
  }
}
