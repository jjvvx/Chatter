<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a Telegram result that's simply one value, like a boolean or
 * integer.
 */
class NonResult{

  /** @var mixed */
  private $value;

  public function __construct($value){
    $this->value = $value;
  }

  /** @return mixed */
  public function getValue(){
    return $this->value;
  }
}
