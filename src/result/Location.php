<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a geographical location.
 */
class Location extends Result{

	protected $latitude;
	protected $longitude;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->latitude  = $json['latitude'];
		$this->longitude = $json['longitude'];
	}

	public function getLatitude(): float{
		return $this->latitude;
	}

	public function getLongitude(): float{
		return $this->longitude;
	}
}
