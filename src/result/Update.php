<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a Telegram update.
 */
class Update extends Result{

	protected $type='unknown';
	protected $ID=0;
	protected $content;

	public function __construct($json){
		$json = $this->parseJSON($json);
		if(isset($json['message'])){
			$this->type = 'message';
			$this->content = new Message($json['message']);
		}elseif(isset($json['edited_message'])){
			$this->type = 'edited_message';
			$this->content = new Message($json['edited_message']);
		}elseif(isset($json['channel_post'])){
			$this->type = 'channel_post';
			$this->content = new Message($json['channel_post']);
		}elseif(isset($json['edited_channel_post'])){
			$this->type = 'edited_channel_post';
			$this->content = new Message($json['edited_channel_post']);
		}else
			$this->content = new Unknown($json);
		$this->ID = $json['update_id'];
	}

	public function getType(): string{
		return $this->type;
	}

	public function getID(): int{
		return $this->ID;
	}

	public function getContent(): Result{
		return $this->content;
	}

}
