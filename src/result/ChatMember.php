<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a chat member in a group.
 */
class ChatMember extends Result{

  protected $User;
  protected $status='';

  public function __construct($json){
		$json = $this->parseJSON($json);
    $this->User   = new User($json['user']);
    $this->status = $json['status'];
  }

  /**
   * Gets the chat member as a user.
   */
  public function getUser(): User{
    return $this->User;
  }

  /**
   * Gets the chat member's status (administrator, banned, etc.)
   */
  public function getStatus(): string{
    return $this->status;
  }
}
