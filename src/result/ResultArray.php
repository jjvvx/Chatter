<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

use Iterator;

/**
 * A Telegram result that's an array of other objects.
 */
abstract class ResultArray implements Iterator{

  protected $array    = [];
	protected $position = 0;

  /**
   * Iterator constructor.
   *
   * @param array $array - Result array.
   * The base array should not be associative ({} in JSON), but rather indexed
   * ([] in JSON, commonly called a list.) Each entry must be turned into a
   * Result object (array of PhotoSize, array of Update, etc.)
   */
  public abstract function __construct(array $array);

  public final function current(){
    return $this->array[$this->position] ?? null;
  }

  public final function key(){
    return $this->position;
  }

  public final function rewind(){
    $this->position = 0;
  }

  public final function next(){
    ++$this->position;
  }

  public function valid(): bool{
    return isset($this->array[$this->position]);
  }
}
