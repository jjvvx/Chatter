<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a Telegram chat.
 */
class Chat extends Result{

	protected $ID;
	protected $type;

	protected $title;

	protected $userName;
	protected $firstName;
	protected $lastName;
	protected $fullName;

	protected $areAllAdmin = false;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->ID   = $json['id'];
		$this->type = $json['type'];

		if(isset($json['username']))
			$this->userName = $json['username'];
		if(isset($json['all_members_are_administrators']))
			$this->areAllAdmin = $json['all_members_are_administrators'];
		if($this->type == 'private'){
			$this->firstName = $this->fullName = $json['first_name'];
			if(isset($json['last_name'])){
				$this->lastName = $json['last_name'];
				$this->fullName .= ' '.$this->lastName;
			}
		}else{
			$this->title = $json['title'];
		}
	}

	/**
	 * @return string
	 */
	public function getType(){
		return $this->type;
	}

	public function getID(){
		return $this->ID;
	}

	/**
	 * @return string|null
	 */
	public function getTitle(){
		return $this->title;
	}

	/**
	 * @return string|null
	 */
	public function getUsername(){
		return $this->userName;
	}

	/**
	 * Gets the user's name if this is a private chat.
	 * 
	 * @param int $type Whether to return the first name, last name or full name.
	 * @return string
	 */
	public function getName(int $type = self::FIRST_NAME): string{
		if($type == self::FIRST_NAME)
			return $this->firstName;
		elseif($type == self::LAST_NAME)
			return $this->lastName;
		elseif($type == self::FULL_NAME)
			return $this->fullName;
		else
			throw new TelegramException(
				'Argument in '.__METHOD__.' must be between 0 and 2'
			);
	}

	/**
	 * Checks if the group is set so that everyone's an administrator.
	 * 
	 * @return bool
	 */
	public function areAllAdmin(){
		return $this->areAllAdmin;
	}
}
