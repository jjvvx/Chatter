<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Contains multiple resolutions of a photo in Telegram.
 */
class Photo extends ResultArray{

	public function __construct(array $photos){
		foreach($photos as $photo)
			$this->array[] = new PhotoSize($photo);
	}
}
