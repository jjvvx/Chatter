<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;
use JoaquinV\Chatter\TelegramException;
use stdClass;

/**
 * Abstract class that most result representation objects extend.
 */
abstract class Result{

	protected $json;

	/**
	 * Object constructor.
	 *
	 * @param mixed $json - JSON-serialized object. Can be:
	 *  string:   JSON string.
	 *  stdClass: Decoded JSON as an object (slower, not recommended).
	 *  array:    Decoded JSON as an array.
	 */
	public abstract function __construct($json);

	/**
	 * Parses JSON input and sets
	 *
	 * @param mixed $js - See Result::__construct
	 *
	 * @return stdClass - Decoded JSON as an object.
	 *
	 * This method turns JSON arrays into an object internally, making it slower.
	 */
	protected final function parseJSON($js): array{
		switch(gettype($js)){
			case 'string':
				$this->json = $js;
				$js = json_decode($js, true);
				if(!is_array($js))
					throw new TelegramException('JSON invalid.');
				return $js;
			break;
			case 'array':
				$this->json = json_encode($js);
				return $js;
			break;
			case 'object':
				if(!($js instanceof stdClass))
					throw new TelegramException('JSON invalid.');
				$this->json = json_encode($js);
				return json_decode($js, true);
			break;
			default:
				throw new TelegramException('JSON invalid.');
			break;
		}
	}

	/**
	 * Gets JSON-serialized version of this Telegram object.
	 *
	 * @return string - JSON
	 */
	public final function getJSON(): string{
		return $this->json;
	}
}
