<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents unknown output of the Telegram API.
 */
class Unknown extends Result{

	public function __construct($json){
		$json = $this->parseJSON($json);
	}
}
