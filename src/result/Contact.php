<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter\result;

/**
 * Represents a contact.
 */
class Contact extends Result{

	protected $mobileNumber='';
	protected $firstName='';
	protected $lastName;
	protected $userID;

	public function __construct($json){
		$json = $this->parseJSON($json);
		$this->mobileNumber = $json['phone_number'];
		$this->firstName = $json['first_name'];
		if(isset($json['last_name']))
			$this->lastName = $json['last_name'];
		if(isset($json['user_id']))
			$this->userID = $json['user_id'];
	}

	/**
	 * Gets this contact's phone number as a string.
	 * @return string
	 */
	public function getNumber(): string{
		return $this->mobileNumber;
	}

	public function getName(&$first='', &$last=''): string{
		$first = $full = $this->firstName;
		$last  = $this->lastName;
		if($last)
			$full .= ' '.$last;
		return $full;
	}

	public function getID(){
		return $this->userID;
	}
}
