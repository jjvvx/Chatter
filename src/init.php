<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

// This file includes all files and defines some constants.

namespace JoaquinV\Chatter;



const NAME      = 'Chatter';
const SOURCE    = 'https://github.com/Joaquin-V/Chatter';
const AUTHOR    = 'Joaquín Varela';
const WEBSITE   = 'N/A';
const LICENSE   = 'GPL v3';
const COPYRIGHT = "Copyright (C) ".AUTHOR." 2016";
