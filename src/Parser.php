<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter;

/**
 * Helper class to determine which Result object should be used when receiving
 * a response from the Telegram API.
 */
class Parser{

	/**
	 * Parses the raw output of the API and returns a result representation.
	 * 
	 * @param string $result The output of the API.
	 * @param string $method The method name used to obtain this output.
	 * @param string $hook The type of hook this result is relevant to.
	 * 
	 * @return object
	 */
	public static function parseRaw(string $result, string $method, &$hook){
		$json = json_decode($result, true);
		// If the ok value is false, construct a TelegramError.
		if(!$json['ok'])
			return new result\Error($json);

		// One by one, start seeing what the output could be.
		switch(strtolower($method)){
			// getMe
			case 'getme':
				$hook = 'user';
				return new result\User($json['result']);
			break;
			// getUpdate
			case 'getupdates':
				$hook = 'update';
				return new result\Updates($json['result']);
			break;
			// Message sending functions.
			case 'sendmessage':
			case 'sendphoto':
			case 'sendaudio':
			case 'senddocument':
			case 'sendsticker':
			case 'sendvideo':
			case 'sendvoice':
			case 'sendlocation':
			case 'forwardmessage':
				$hook = 'message';
				return new result\Message($json['result']);
			break;
			// getFile
			case 'getfile':
				$hook = 'file';
				return new result\File($json['result']);
			break;
			// getWebhookInfo
			case 'getwebhookinfo':
				return new result\WebhookInfo($json['result']);
			// getChat
			case 'getchat':
				$hook = 'chat';
				return new result\Chat($json['result']);
			break;
			// getChatMember
			case 'getchatmember':
				$hook = 'chatmember';
				return new result\ChatMember($json['result']);
			break;
			// NonResult cases
			case 'setwebhook':
			case 'deletewebhook':
			case 'sendchataction':
			case 'kickchatmember':
			case 'unbanchatmember':
			case 'leavechat':
			case 'answercallbackquery':
			case 'gerchatmemberscount':
				return new result\NonResult($json['result']);
			break;
			// getUserProfilePhotos
			case 'getuserprofilephotos':
				return new result\UserProfilePhotos($json['result']);
			// If it's none of these, send an Unknown object.
			default:
				return new result\Unknown($json['result']);
			break;
		}
	}
}
