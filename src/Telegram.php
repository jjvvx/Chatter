<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter;

// @author Joaquin-V/Chatter - Telegram bot/API.
// @site   http://github.com/Joaquin-V/Chatter
// Telegram - Main Telegram object.

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

use JoaquinV\Chatter\result\Error;
use JoaquinV\Chatter\result\File;
use JoaquinV\Chatter\result\User;

/**
 * Represents a Telegram bot.
 */
class Telegram{

	// Telegram API URLs.
	const URI_FORMAT        = 'https://api.telegram.org/bot%s/';
	const FILE_DOWNLOAD_URI = 'https://api.telegram.org/file/bot%s/%s';

	/**
	 * Bot token
	 * @var string
	 */
	private $token = '';

	/**
	 * GuzzleHttp client
	 * @var Client
	 */
	private $Client;

	/**
	 * Cached User object of the bot.
	 * @var User
	 */
	private $me;

	/**
	 * PSR logger
	 * @var LoggerInterface
	 */
	private $Log;

	private $hooks = [
		'update'     => [],
		'message'    => [],
		'user'       => [],
		'chat'       => [],
		'file'       => [],
		'chatmember' => [],
		'error'      => []
	];

	/**
	 * Object constructor.
	 *
	 * @param string          $token Bot token from BotFather.
	 * @param LoggerInterface $log   Logger to use.
	 *
	 */
	public function __construct(string $token, LoggerInterface $Log=null){
		$this->token = $token;
		$this->Client = new Client([
			'base_uri' => sprintf(self::URI_FORMAT, $token),
			'http_errors' => false, // Telegram returns HTTP errors corresponding to JSON-seralised errors.
			'curl'     => [
				CURLOPT_SSL_VERIFYPEER => false
			]
		]);

		$this->Log = $Log;
	}

	/**
	 * Run a method and return direct HTTP output, including the `ok` key.
	 *
	 * @param Method $Method - Method to run.
	 *
	 * @return string of JSON result (or any non-JSON output that may occur)
	 *
	 */
	public function methodRaw(Method $Method): string{
		$Client   = $this->Client;
		$Response = $Client->request('POST',
			$Method->getMethod(),
			self::mkGuzzleMultipart($Method->getParameters())
		);

		return $Response->getBody();
	}

	/**
	 * Run a method and return a parsed result.
	 *
	 * @param Method $Method - Method to run.
	 *
	 * @return Either a Result or ResultArray, depending on the method.
	 *
	 * @throws TelegramException on error if said option was enabled.
	 */
	public function method(Method $Method){
		$Response = $this->methodRaw($Method);
		$Result   = Parser::parseRaw($Response, $Method->getMethod(), $hook);

		// Hooks.
		if(!empty($hook)){
			foreach($this->hooks[$hook] as $h)
				$h(
					strtolower($Method->getMethod()),
					$Result
				);
		}

		if($Result instanceof result\Error)
			$this->logError($Result);

		return $Result;
	}

	/** Bind self::method() to self::__invoke() for shorter code. */
	public function __invoke(Method $Method){
		return $this->method($Method);
	}

	public static function mkGuzzleMultipart(array $params): array{
		if(empty($params))
			return [];
		$guzzle = [];
		foreach($params as $param => $value){
			// Process InputFile separately.
			if($value instanceof InputFile)
				$guzzle[] = $value->getFormParam($param);
			else
				$guzzle[] = [
					'name'     => $param,
					'contents' => $value
				];
		}

		return [
			'multipart' => $guzzle
		];
	}

	/**
	 * Adds a hook to receive results.
	 *
	 * @param string $type - Type of results to receive. Can be:
	 *  - update     (on getUpdate, returns Updates)
	 *  - message    (on send*, returns Message)
	 *  - user       (on getMe, returns User)
	 *  - chat       (on getChat, returns Chat)
	 *  - file       (on getFile, returns File)
	 *  - chatmember (on getChatMember, returns ChatMember; on
	 *  getChatAdministrators, returns ChatMembers)
	 *  - error      (whenever an error occurs, returns Error)
	 * 
	 *  All classes mentioned are relative to the Chatter\result namespace.
	 *
	 * @param callable $callable Function to run when result of $type is found.
	 * The function must take two arguments in this order:
	 * - `string`             Method used in the API call.
	 * - `Result|ResultArray` Response from the API.
	 *
	 * @return bool - True if $callable is callable, false if not.
	 */
	public function addHook(string $type, $callable): bool{
		if(!is_callable($callable))
			return false;
		$this->hooks[$type][] = $callable;
		return true;
	}

	/**
	 * Logs an error
	 *
	 * @param result\Error $Error  - An Error result.
	 * @param Method       $Method - Method for context.
	 *
	 * @return void
	 */
	public function logError(Error $Error, Method $Method=null){
		$msg = sprintf(
			"An error was thrown during a Telegram API call.\n".
			"Error code %d: %s",
			$Error->getCode(), $Error->getDescription()
		);

		if($Method){
			$msg.= "\nMethod ran: ".$Method->getMethod();

			if(!empty($Method->getParameters())){
				$argStr = PHP_EOL;
				foreach($Method->getParameters() as $param => $value){
					if($value instanceof InputFile)
						$value = 'InputFile';
					$argStr.= sprintf("%s: %s\n", $param, $value);
				}
				$msg.= $argStr;
			}
		}
		return $msg;
	}

	/**
	 * Sends a getMe request and caches the result.
	 *
	 * @param bool $update Whether or not to force fetching a new User object
	 * rather than a cached object.
	 *
	 * @return User object of bot.
	 *
	 * @throws TelegramException if an error occured.
	 */
	public function getMe(bool $update=false){
		if($update || $this->me == null){
			$Result = $this->method(new Method('getMe'));
			if($Result instanceof Error)
				throw $Result->mkException();
			$this->me = $Result;
		}else
			$Result = $this->me;

		return $Result;
	}

	/**
	 * Downloads a file on disk.
	 * 
	 * @param string $fileid Telegram file ID.
	 * @param string $tmp    Temporary directory. (defaults to /tmp)
	 * @param string $mode   Mode to give the file.
	 */
	public function downloadFile(
		string $fileid, string $tmp='/tmp', string $mode='r'
	){
		$Method = new Method('getFile', ['file_id'=>$fileid]);
		$File = $this->method($Method);

		if($File instanceof File){
			$url = sprintf(self::FILE_DOWNLOAD_URI, $this->token, $File->getPath());
			$tmpfile = tempnam($tmpdir, 'tgdownload-');
			$this->Client->get($url, ['save_to'=>$tmpfile]);
			return fopen($tmpfile, $mode.'b');
		}else
			throw new TelegramException('An error occured while downloading file.');
	}
}
