<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter;

/**
 * Represents an input file as a method parameter.
 */
class InputFile{

  private $type;
  private $file;

  private $name;

  /**
   * Object constructor.
   *
   * @param string $type - Type of input.
   *  'local':  File path on disk.
   *  'stream': File stream.
   *  'fileid': Telegram file ID.
   *  'url':    External URL.
   *  'string': Content string. (useful for sending .txt files)
   *
   * @param mixed $file - File. Can be:
   *  string if the type is string, fileid, local or url
   *  stream if the type is stream
   */
  public function __construct(string $type, $file){
    $this->type = $type;
    $this->file = $file;
  }

  /**
   * Sets the filename.
   *
   * @param string $name - File's name.
   *  Telegram, much like Windows, cares about the file's extension so be sure
   *  to include it (e.g. 'picture.jpg' for JPEGs instead of 'picture'). No
   *  extension defaults to the mimetype being text/plain.
   *
   */
  public function setName(string $name){
    $this->name = $name;
  }

  /**
   * Gets a GuzzleHttp compatible form parameter.
   *
   * @param string $param - The parameter name.
   *
   * @return array to add to the 'form_params' key of the third argument of
   *  GuzzleHttp\Client::request().
   */
  public function getFormParam(string $param): array{
    $guzzle = [
      'name'     => $param,
      'contents' => $this->getContents()
    ];
    if(isset($this->name))
      $guzzle['filename'] = $this->name;

    return $guzzle;
  }

  /**
   * Get the contents of the file.
   * **Note**: if the input file is a local disk file or a stream, this
   * function will return a stream/file resource. If the input file is a URL
   * or file ID, it won't output what Telegram will present to the end user.
   * 
   * @return resource|string
   */
  public function getContents(){
    switch($this->type){
      // In the case of local, return a binary stream of the file.
      case 'local':
        return fopen($this->file, 'rb');
      break;
      // In the case of a URL, stream, string or file ID, return the contents raw.
      case 'stream':
      case 'fileid':
      case 'url':
      case 'string':
        return $this->file;
      break;
      default:
        throw new TelegramException('Unknown InputFile type: '.$this->type);
      break;
    }
  }
}
