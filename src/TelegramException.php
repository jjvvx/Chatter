<?php

// Joaquin-V/Chatter - Telegram API for PHP 7.
// License: GNU General Public License <http://www.gnu.org/licenses/>
// Copyright (C) 2016 Joaquín Varela

namespace JoaquinV\Chatter;
use Exception;

/**
 * Exception for Telegram-related errors and issues.
 */
class TelegramException extends Exception{

}
