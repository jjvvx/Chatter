# Chatter
### Telegram API written in PHP

This is a bot API I made in my free time in PHP. It's very lightweight and doesn't have any dependencies other than libcurl.

## Installation

This library is on Packagist as `joaquin-v/chatter`

## How to use it.

To start a Telegram instance:
```php
namespace JoaquinV\Chatter;

$Telegram = new Telegram(BOT_TOKEN);
```

This will throw a `TelegramException` if it fails to use Telegram's `getMe` method, signaling an authentication error.

Sending methods is done through object-oriented programming.

```php
// Send a text message to a group.
$Method = new Method('sendMessage', ['chat_id'=>-12345678, 'text'=>'Hello, world!']);
$Telegram->method($Method);
//// Or, alternatively
// $Telegram($Method);

// Send an MP3 to a channel.
$Method = new Method('sendAudio', ['chat_id'=>'@musicchannel']);
$Method->setParameter('performer', 'Royal Blood');
$Method->setParameter('title', 'Hook, Line and Sinker');
$Method->setParameter('audio', new InputFile('local', '/path/to/file.mp3') );

$Telegram->method($Method);

```

For documentation, please be patient as I write it.

## Copyright

Copyright (C) 2016 Joaquín Varela

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
